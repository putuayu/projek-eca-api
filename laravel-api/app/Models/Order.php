<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'code_barang', 'warna','harga_satuan', 'ukuran', 'harga_total','total', 'role', 'payment', 'id_stok', 'nama_pembeli', 'instansi', 'keterangan'
    ];


    protected $table = 'order';
}
