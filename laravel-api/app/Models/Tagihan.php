<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tagihan extends Model
{
    protected $fillable = [
        'id_pesanan', 'code_barang', 'warna','ukuran','harga_satuan', 'harga_total', 'total', 'role','no_angsuran','id_stok', 'nama_pembeli', 'instansi', 'keterangan', 'foto_kwitansi','total_tagihan','DP'
    ];

    protected $casts = [
        'foto_kwitansi' => 'array'
    ];

    protected $table = 'tagihan';
}
