<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Stok extends Model
{
    protected $fillable = [
        'code_barang', 'warna','ukuran','harga','total', 'role'
    ];


    protected $table = 'stok';
}
