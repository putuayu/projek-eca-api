<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Tagihan;
use App\Models\Order;
use Auth;
use JWTAuth;

class TagihanController extends Controller
{
    public function createBill(Request $request,$id)
    {
        $order = Order::find($id);
        $dp = $request->dp;
        $totalTagihan = $order->harga_total - $dp;
        if($order->payment != 3){
            return response()->json([
                'error' => TRUE,
                'message' => 'Pesanan ini tidak memiliki tagihan',
                'code' => 200
            ],200);
        }else{
            $tagihan = Tagihan::create([
                'id_pesanan'    => $order->id,
                'code_barang'   => $order->code_barang,
                'warna'         => $order->warna,
                'ukuran'        => $order->ukuran,
                'harga_satuan'  => $order->harga_satuan,
                'harga_total'   => $order->harga_total,
                'DP'            => $dp,
                'total_tagihan' => $totalTagihan,
                'total'         => $order->total,
                'role'          => $order->role,
                'nama_pembeli'  => $order->nama_pembeli,
                'instansi'      => $order->instansi,
                'keterangan'    => $order->keterangan,
                'foto_kwitansi' => $request->foto_kwitansi,
                'no_angsuran'   => $request->no_angsuran,
            ]);
            return $tagihan;
        }    
              
    }

    public function showAllBill(){
        $user  = Auth::user();
        $role = $user->role;
        if($role == 1){
            return Tagihan::all();
        }else{
            try {
                $soal = Tagihan::where('role',$role)->firstOrFail();
                return Tagihan::where('role',$role)->get()->toArray();
            } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
                $soal = json_encode(['error' => 'Tidak Ada Hasil']);
                return $soal;
            }
        } 
    }

    public function updateBill(Request $request, $id)
    {   
        $data = Tagihan::where('id', $id)->first();
        $kwitansi = $data['foto_kwitansi'];
        $new = array_push($kwitansi,$request->foto_kwitansi);
        $stok = Tagihan::where('id', $id)->update([
            'foto_kwitansi' => $kwitansi,
            'total_tagihan' => $request->total_tagihan,
            'no_angsuran'   => $request->no_angsuran,
        ]);

        return $data;
    }

    public function deleteOrder(Request $request, $id)
    {
        $user  = Auth::user();
        $role = $user->role;
        $tagihan = Tagihan::Where([
            ['id', $id], 
            ['role',$role]
        ])->first();
        
        if($tagihan != null){
            $tagihan->delete();
            return json_encode(['Success' => 'Tagihan Telah di hapus']);
        }else{
            return json_encode(['Error' => 'Tagihan tidak ditemukan']);
        }
        
        
    }
}
