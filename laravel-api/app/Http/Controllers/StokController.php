<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Stok;
use Illuminate\Http\Request;
use Auth;
use JWTAuth;

class StokController extends Controller
{
    public function createStok(Request $request)
    {
        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }

        $user = JWTAuth::parseToken()->authenticate();
        $role = $user->role;
        $stok = Stok::create([
            'code_barang'    => $request->code_barang,
            'warna'    => $request->warna,
            'harga'    => $request->harga,
            'total'    => $request->total,
            'role'     => $role,
            'ukuran'    => $request->ukuran,
        ]);
        $role = $this->showAllStok();
        return $stok;
    }

    public function showAllStok(){
        $user  = Auth::user();
        $role = $user->role;
        if($role == 1){
            return Stok::all();
        }
        else{
            try {
                $stok = Stok::where('role',$role)->firstOrFail();
                return Stok::where('role',$role)->get()->toArray();
            } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
                return response()->json([
                    'error' => TRUE,
                    'message' => 'Tidak ada Stok',
                    'code' => 404
                ],404);
            }
        }
    }

    public function showDetailStok($id){
        $user  = Auth::user();
        $role = $user->role;
        try {
            return Stok::where('id',$id)->firstOrFail();
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json([
                'error' => TRUE,
                'message' => 'Tidak ada Stok',
                'code' => 404
            ],404);
        }
    }

    public function updateStok(Request $request, $id)
    {
        $stok = Stok::where('id', $id)->update([
            'warna'    => $request->warna,
            'harga'    => $request->harga,
            'total'    => $request->total,
            'ukuran'    => $request->ukuran,
        ]);

        return $request;
    }

    public function deleteStok(Request $request, $id)
    {
        $user  = Auth::user();
        $role = $user->role;
        $stok = Stok::find($id && $role)->delete();
        try {
            return json_encode(['Success' => 'Stok Telah di hapus']);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            $message = json_encode(['error' => 'Tidak Ada Hasil Stok']);
            return $message;
        }
    }
}
