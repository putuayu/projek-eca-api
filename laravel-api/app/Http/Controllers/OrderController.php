<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Stok;
use App\Models\Order;
use Auth;
use JWTAuth;


class OrderController extends Controller
{
    public function createOrder(Request $request,$id)
    {
        $stok = Stok::find($id);
        $total = $stok->total;
        $order = $request->order;
        $totalHarga = $stok->harga * $order;
        if($total == 0 || $total - $order < 0){
            return response()->json([
                'error' => TRUE,
                'message' => 'Tidak ada Pesanan',
                'code' => 404
            ],404);
        }else{
            $totalNew = Stok::where('id', $id)->update(['total' => $total - $order]);
            $ordering = Order::create([
                'id_stok'  => $id,
                'code_barang'    => $stok->code_barang,
                'warna'    => $stok->warna,
                'harga_satuan' => $stok->harga,
                'harga_total' => $totalHarga,
                'ukuran' => $request->ukuran,
                'total'    => $order,
                'role'     => $stok->role,
                'payment'  => $request->payment,
                'nama_pembeli' =>$request->nama_pembeli,
                'instansi'  => $request->instansi,
                'keterangan' => $request->keterangan

            ]);
            return $ordering;
        }    
    }

    public function showAllOrder(){
        $user  = Auth::user();
        $role = $user->role;
        try {
            $soal = Order::where('role',$role)->firstOrFail();
            return Order::where('role',$role)->get()->toArray();
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json([
                'error' => TRUE,
                'message' => 'Tidak ada Pesanan',
                'code' => 404
            ],404);
        }
    }

    public function showDetailOrder($id){
        $user  = Auth::user();
        $role = $user->role;
        try {
            return Order::where('id',$id)->first();      
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json([
                'error' => TRUE,
                'message' => 'Tidak ada Pesanan',
                'code' => 404
            ],404);
        }
    }

    public function updateOrder(Request $request, $id)
    {
        $stok = Order::where('id', $id)->update([
            'warna'    => $request->warna,
            'harga'    => $request->harga,
            'total'    => $request->total,
            'payment'  => $request->payment,
            'nama_pembeli' =>$request->nama_pembeli,
            'instansi'  => $request->instansi,
            'keterangan' => $request->keterangan,
            'ukuran' => $request->ukuran
        ]);

        return $request;
    }

    public function deleteOrder(Request $request, $id)
    {
        $user  = Auth::user();
        $role = $user->role;
        $order = Order::Where([
            ['id', $id], 
            ['role',$role]
        ])->first();

        if($order != null){
            $order->delete();
            return json_encode(['Success' => 'Stok Telah di hapus']);
        }   
        
        return $order;
    }

    public function getMontlyOrder($year, $month)
    {

        $user  = Auth::user();
        $role = $user->role;
        if($role == 1){
            $order = Order::whereYear('created_at', $year)
            ->whereMonth('created_at', $month)
            ->get();
            return $order;
        }else{
            try {
                $order = Order::where('role',$role)
                ->whereYear('created_at', $year)
                ->whereMonth('created_at', $month)
                ->get();
                return $order;
            } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
                return response()->json([
                    'error' => TRUE,
                    'message' => 'Tidak ada Pesanan',
                    'code' => 404
                ],404);
            }
        }
    }

    public function getSumMonthly($year, $month){
        $user  = Auth::user();
        $role = $user->role;
        if($role == 1){
            $order = Order::whereYear('created_at', $year)
            ->whereMonth('created_at', $month)
            ->get();
            $sum = $order->sum('harga_total');
            return  response()->json([
                'Hasil Bulan ini' => $sum,
                'code' => 200
            ],200);
        }else{
            try {
                $order = Order::where('role',$role)
                ->whereYear('created_at', $year)
                ->whereMonth('created_at', $month)
                ->get();
                $sum = $order->harga_total->sum('harga_total');
                return  $sum;
            } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
                return response()->json([
                    'error' => TRUE,
                    'message' => 'Tidak ada Pesanan',
                    'code' => 404
                ],404);
            }
        }
    }
}
