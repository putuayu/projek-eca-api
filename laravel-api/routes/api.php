<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'App\Http\Controllers\UserController@register');
Route::post('login', 'App\Http\Controllers\UserController@login');
Route::get('user', 'App\Http\Controllers\UserController@getAuthenticatedUser')->middleware('jwt.verify');
Route::get('logout', 'App\Http\Controllers\UserController@logout')->name('api.jwt.logout');

Route::post('stok/insert', 'App\Http\Controllers\StokController@createStok')->middleware('jwt.verify');
Route::get('stok/show', 'App\Http\Controllers\StokController@showAllStok')->middleware('jwt.verify');
Route::get('stok/show/{id}', 'App\Http\Controllers\StokController@showDetailStok')->middleware('jwt.verify');
Route::post('stok/update/{id}', 'App\Http\Controllers\StokController@updateStok')->middleware('jwt.verify');
Route::post('stok/delete/{id}', 'App\Http\Controllers\StokController@deleteStok')->middleware('jwt.verify');

Route::post('pesanan/insert/{id}', 'App\Http\Controllers\OrderController@createOrder')->middleware('jwt.verify');
Route::get('pesanan/show', 'App\Http\Controllers\OrderController@showAllOrder')->middleware('jwt.verify');
Route::get('pesanan/show/{id}', 'App\Http\Controllers\OrderController@showDetailOrder')->middleware('jwt.verify');
Route::get('pesanan/monthly/show/{year}/{month}', 'App\Http\Controllers\OrderController@getMontlyOrder')->middleware('jwt.verify');
Route::get('pesanan/monthly/show/{year}/{month}/sum', 'App\Http\Controllers\OrderController@getSumMonthly')->middleware('jwt.verify');
Route::post('pesanan/update/{id}', 'App\Http\Controllers\OrderController@updateOrder')->middleware('jwt.verify');
Route::post('pesanan/delete/{id}', 'App\Http\Controllers\OrderController@deleteOrder')->middleware('jwt.verify');

Route::post('tagihan/insert/{id}', 'App\Http\Controllers\TagihanController@createBill')->middleware('jwt.verify');;
Route::get('tagihan/show', 'App\Http\Controllers\TagihanController@showAllBill')->middleware('jwt.verify');;
Route::post('tagihan/update/{id}', 'App\Http\Controllers\TagihanController@updateBill')->middleware('jwt.verify');;
Route::post('tagihan/delete/{id}', 'App\Http\Controllers\TagihanController@deleteOrder')->middleware('jwt.verify');;
