<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Order extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order', function (Blueprint $table) {
            $table->id();
            $table->string('code_barang');
            $table->string('warna');
            $table->string('ukuran');
            $table->integer('harga_satuan');
            $table->integer('harga_total');
            $table->integer('total');
            $table->integer('role');
            $table->integer('payment');
            $table->string('id_stok');
            $table->string('nama_pembeli');
            $table->string('instansi');
            $table->string('keterangan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
