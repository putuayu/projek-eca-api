<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Tagihan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tagihan', function (Blueprint $table) {
            $table->id();
            $table->integer('id_pesanan')->unique();
            $table->string('code_barang');
            $table->string('warna');
            $table->string('ukuran');
            $table->integer('harga_satuan');
            $table->integer('harga_total');
            $table->integer('total_tagihan');
            $table->integer('DP');
            $table->integer('total');
            $table->integer('no_angsuran');
            $table->json('foto_kwitansi');
            $table->integer('role');
            $table->string('nama_pembeli');
            $table->string('instansi');
            $table->string('keterangan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
